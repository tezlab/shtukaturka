$(document).ready(function(){
	$('.slider ul').bxSlider({
		slideMargin: 20,
		slideWidth: 190,
		minSlides: 4,
		maxSlides: 4,
		pager: false,
		moveSlides: 4
	});

	var autoScroll = false;

	$('.main-menu a').click(function(){
		var el = $(this);
		var block = el.attr('id').replace('link-','block-');
		$('.main-menu a').removeClass('active');
		el.addClass('active');
		autoScroll = true;
		$(window).scrollTo($('#'+block), 1000, {
			easing:'easeInOutExpo',
			onAfter: function(){
				autoScroll = false;
			}
		});
		return false;
	});

	var heights = [];

	$('.main-menu a').each(function(){
		var block = $('#'+$(this).attr('id').replace('link-','block-'));
		if(block.length){
			heights.push({height: block.offset().top, item: $(this).attr('id')});
		}
	});
	var curActive = null;
	var newActive = null;
	if (heights.length > 0){
		$(window).scroll(function(){
			var curScr = $(window).scrollTop();
			for (var i = 0; i < heights.length; i++ ){
				if (curScr +200 > heights[i].height){
					newActive = i;
				}
			}
			if (newActive != curActive && !autoScroll)	{
				curActive = newActive;
				$('.main-menu a').removeClass('active');
				$('#'+heights[curActive].item).addClass('active');
			}
		});
		$(window).scroll();
	}

	$('.call-order-trigger').on('click', function(){
		$('#call-order-trigger').click();
	});

	$('#call-order-trigger').fancybox({
		width: 300,
		height: 400,
		autoSize: false,
		padding: 25,
		autoResize: false
	});

	$('.save').on('click',function(){
		var form = $(this).parents('form');
		$.post('/lib/order.php',form.serializeArray(),function(data){
			if (data.status == 'success'){
				form.find('input:not([name=action]),textarea').val('');
				form.find('.form-container').html('<p class="success">Заявка успешно отправлена</p>')
				setTimeout('$.fancybox.close()',2000);
			} else {
				form.find('input,textarea').removeClass('error-input');
				if (data.fields && data.fields.length > 0){
					for (var i=0;i<data.fields.length;i++){
						form.find('[name='+data.fields[i]+']').addClass('error-input');
					}
				}
			}
			$.fancybox.update();
		},'json');
		return false;
	});

	$(document).ready(function(){
		$('#slides img').load(function(){
			$('#slides').css({'visibility' : 'visible'});
		});
		var slider1 = $('#slides').bxSlider2({
			onBeforeSlide: function(current){
				$('#slides li').removeClass('active');
			},
			onAfterSlide: function(current){
				$('#slides li[rel='+current+']').addClass('active');
			},
			controls: false
		});
		var slider2 = $('#slides-small').bxSlider2({displaySlideQty: 6,moveSlideQty : 6});
		$('#slides-small li').on('click',function(){
			var i = $(this).attr('rel');
			if (!$('#slides li[rel='+i+']').hasClass('active')){
				slider1.goToSlide(parseInt(i));
			}
			return false;
		});
		$('#slides li').on('click',function(){
			var i = $(this).attr('rel');
			if (!$('#slides li[rel='+i+']').hasClass('active')){
				slider1.goToSlide(parseInt(i));
			}
			return false;
		});
		$(window).resize(function(){
			var w = ($('.page-wrapper').width() - 960)/2;
			$('.small-photos .bx-next').width(w).css('right','-'+w+'px');
			$('.small-photos .bx-prev').width(w).css('left','-'+w+'px');
		});
		$(window).resize();
	});
});
