<?php
function pre(){
	echo '<pre>';
	print_r(func_num_args() > 1 ? func_get_args() : func_get_arg(0));
	echo '</pre>' . "\n";
}
$emails = array('waterman_85@mail.ru', 'nntezlab+partner@gmail.com');
$site = 'Штукатурка стен';
$subject = 'Сообщение с сайта';

function sendmail($text){
	global $site,$subject,$emails;
	require_once('PHPMailer/class.phpmailer.php');
	
	$mail = new PHPMailer(true);
	
	$mail->From     = "noreply@".$_SERVER['SERVER_NAME'];
	$mail->FromName = iconv('UTF-8','WINDOWS-1251',$site);
	$mail->Subject  = iconv('UTF-8','WINDOWS-1251',$subject);
	$mail->CharSet  = 'Windows-1251';
	$mail->SingleTo	= true;
	$mail->ContentType = 'text/html';
	foreach ($emails as $email){
		$mail->AddAddress($email,$email);
	}
	$template = '
		<html>
			<head><title>%s</title></head>
			<body>
				<h3>%s</h3>
				%s
			</body>
		</html>
	';
	$mail->MsgHTML(iconv('UTF-8','WINDOWS-1251',sprintf($template, $subject, $subject, $text)));
	$mail->Send();
}

$result = array('status' => 'error','message' => '');
if (!empty($_SERVER['HTTP_REFERER'])){
	$referer = parse_url($_SERVER['HTTP_REFERER']);
	if ($referer['host'] != $_SERVER['HTTP_HOST']){
		$result['message'] = 'Bad referer';
	} else {
		$result['fields'] = array();
		switch($_POST['action']){
			case "call":
				$result['status'] = 'success';
				$name = @htmlspecialchars(strip_tags($_POST['name']));
				$phone = @htmlspecialchars(strip_tags($_POST['phone']));
				$comment = @htmlspecialchars(strip_tags($_POST['comment']));
				if (empty($name)){
					$result['status'] = 'error';
					$result['fields'][] = 'name'; 
				}
				if (empty($phone)){
					$result['status'] = 'error';
					$result['fields'][] = 'phone'; 
				}
				if (empty($comment)){
					$result['status'] = 'error';
					$result['fields'][] = 'comment'; 
				}
				if ($result['status'] == 'success'){
					$content = sprintf('
						<p><b>Имя</b>: %s</p>
						<p><b>Телефон</b>: %s</p>
						<p><b>Комментарий</b>: %s</p>
					',$name,$phone,$comment);
					sendmail($content);
				}
				break;
			case "order":
				$result['status'] = 'success';
				$name = @htmlspecialchars(strip_tags($_POST['name']));
				$phone = @htmlspecialchars(strip_tags($_POST['phone']));
				$comment = @htmlspecialchars(strip_tags($_POST['comment']));
				if (empty($name)){
					$result['status'] = 'error';
					$result['fields'][] = 'name'; 
				}
				if (empty($phone)){
					$result['status'] = 'error';
					$result['fields'][] = 'phone'; 
				}
				if (empty($comment)){
					$result['status'] = 'error';
					$result['fields'][] = 'comment'; 
				}
				if ($result['status'] == 'success'){
					$content = sprintf('
						<p><b>Имя</b>: %s</p>
						<p><b>Телефон</b>: %s</p>
						<p><b>Комментарий</b>: %s</p>
					',$name,$phone,$comment);
					sendmail($content);
				}
				break;
		}
	}
} else {
	$result['message'] = 'Bad referer';
}
echo json_encode($result);
die;

